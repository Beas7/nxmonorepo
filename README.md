# NXMonoRepo

Prerequisites:

- Node Latest LTS (18.15.0 or 18.16.0)

### Adding NX global dep:

- `npm i -g nx@latest`

### Create NX Workspace

`npx create-nx-workspace@latest`

steps:

- // workspace name: example (or your preferred name)
- // Integrated monorepo ✅
- // apps ✅
- // no distributed caching ✅

### SetUp Java and Next JS Apps

- `cd example` // or your choosen name in the worskpace setup above

#### Java/Kotlin w. Graddle

- `npm i --save-dev @jnxplus/nx-boot-gradle` // Adds the nx gradle plugin
- `nx g @jnxplus/nx-boot-gradle:init` // Inicializa el wrapper

- `nx g @jnxplus/nx-boot-gradle:application example` // generarte new Java/kotlin Gradle App

- `nx build example` // builds the Java app
- `nx serve example` // Serves the Java App

#### Next JS w. TS

- `npm i --save-dev @nrwl/next` // Adds the next js plugin
- `nx g @nrwl/next:app next-app` // Generar App nueva next JS
- `nx serve next-app` // Servir la app next

### Next steps

- Install the NX Console in VSCode: https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console
- Or in IntelliJ: https://plugins.jetbrains.com/plugin/15101-nx-console-idea
- Read the Docs: https://nx.dev/
